<?php
session_start();
include_once('./checkhttps.php');
require_once('./nerva/library.php');
$nerva_daemon = new Nerva_Library('localhost', WALLET_PORT);
include_once('./header.php');

if(isset($_SESSION['payed_payment_id'])){
    check_for_spend($_SESSION['payed_payment_id']);
}

if(isset($_SESSION['trasfered'])){
    if($_SESSION['trasfered']==true){
        reset_session();
        header( "refresh:10;url=./load.php" );
        echo "<br>Refresh in 10 seconds";
        exit();
    }
}
if(!isset($_SESSION['totalPayedAmount'])){
	$_SESSION['totalPayedAmount']=Null;
}
$remaining=$_SESSION['totalPayedAmount']-$_SESSION['burnt'];
$reward_with_fees=$_SESSION['won']*0.9 + 0.01 + $remaining;
$reward_without_fees=$_SESSION['won']*0.9+$remaining;
$wallet_amount=$nerva_daemon->getbalance();
$real_wallet_amount = $wallet_amount['balance'] / 1000000000000;
$real_amount_rounded = round($real_wallet_amount, 6);

$unlocked_wallet_amount = $wallet_amount['unlocked_balance'] / 1000000000000;
$unlocked_amount_rounded = round($unlocked_wallet_amount, 6);


?>
<html>
    <head>
    <link type="text/css" rel="stylesheet" href="style.css">
       
       <link rel="stylesheet" href="./css/bootstrap.min.css">
      
     <script src="./js/jquery-3.4.0.min.js"></script>
     <script src="./js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
     <script src="./js/bootstrap.min.js"></script>
      <script src="./js/qrcode.min.js"></script>
    <script src="./js/jQueryStepperWidget/stepper.widget.js"></script>
</head>
<body style="height:100%;">
<div class="container">
<h1 class="text-center display-3">Nerva Dice Game (Mainnet)</h1>
<div class="row">
  <div class="col-lg-6">

<?php



if($unlocked_wallet_amount>=$reward_with_fees && $reward_without_fees>0 && !isset($_SESSION['payed_payment_id'])){
    $pay=$reward_without_fees;//$_SESSION['won']+$remaining;
    $pay=round($pay,1);
   
    $payed_payment_id = bin2hex(openssl_random_pseudo_bytes(8));
    $_SESSION['payed_payment_id']=$payed_payment_id;
    $reply=$nerva_daemon->transfer($pay, $_SESSION['recv_add'],$payed_payment_id);   
     
    $sent=$reply['amount']/1000000000000;
    $fees=$reply['fee']/1000000000000;
	$service_fees=$_SESSION['won']*0.1;
    if($sent==$pay && $fees>0){
        echo '<div class="alert alert-info" role="alert">';
        echo "Transfered<br>";
        echo "Amount:".$sent."<br>";
        echo "Service Fees : ".$service_fees."<br>";
        echo "Network Fees : ".$fees."<br>";
        echo '</div>';
        //$nerva_daemon->_print($reply);
        reset_session();

        
        //header( "refresh:15;url=./load.php" );
        echo '<a href="./load.php"><button class="btn btn-primary  my-2">Start Again</button></a>';
        //echo "<br>Refresh in 10 seconds";
    }else{
        
        echo '<div class="alert alert-danger" role="alert">Error! Please try again later. <br>Refresh in 5 seconds</div>';
        header( "refresh:5;url=./game.php" );
    }
    
}else{
    echo '<div class="alert alert-danger" role="alert">Unlocked balance less than withdraw or zero payment. Please try again later. <br>Refresh in 5 seconds</div>';
    header( "refresh:5;url=./game.php" );
}
?>
</div></div>
</body>

</html>