<?php
session_start();
include_once('./checkhttps.php');

if(!isset($_SESSION['recv_add'])){
  header( "refresh:1;url=./load.php" );
}
?>
<html>
  <head>
  <meta charset="UTF-8">

        <link type="text/css" rel="stylesheet" href="style.css">
       
        <link rel="stylesheet" href="./css/bootstrap.min.css">
       
      <script src="./js/jquery-3.4.0.min.js"></script>
      <script src="./js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
      <script src="./js/bootstrap.min.js"></script>
	   <script src="./js/qrcode.min.js"></script>
     <script src="./js/jQueryStepperWidget/stepper.widget.js"></script>
     
</head>
<body>
<div class="container">
<h1 class="text-center display-3">Nerva Dice Game (Mainnet)</h1>
<div class="row">
  <div class="col-lg-6">
  

</div>
</div>

  <div class="row">
      <div class="col-lg-4 my-4">

<h3>Integrated Address to deposite:</h3>
<p id="int_add" class="mb-0 word-wrap"><?php echo $_SESSION['integrated_address'];?></p>
<div id="qrcode"></div>

<br>
<h3>Receive Address:</h3>
<p class="mb-0 word-wrap"><?php echo $_SESSION['recv_add'];?></p>

<?php

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
if(!isset($_SESSION['selected'])){
	$_SESSION['selected']=NULL;
}

if(!isset($_SESSION['selected'])){
	$_SESSION['payed']=NULL;
}

if(!isset($_SESSION['selected'])){
	$_SESSION['won']=NULL;
}

if(!isset($_SESSION['amount_pay'])){
  $_SESSION['amount_pay']=0.1;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(test_input($_POST["amount_pay"])!="" && test_input($_POST["amount_pay"])<=10 && test_input($_POST["amount_pay"])>0){
    $_SESSION['amount_pay'] = test_input($_POST["amount_pay"]);
  }
  if(test_input($_POST["num"])!="" && test_input($_POST["num"])<=6 && test_input($_POST["num"])>0){
    $_SESSION['selected'] = test_input($_POST["num"]);
  }
  
  //header( "refresh:0;url=./game.php" );
}
?>
<hr>
<p>
  Service Fees of 1% is charged from the won amount when withdrawing to maintain the server. Any time you can click withdraw and exit the game which will transfer balance remaining and win amount to receive address.
</p>
<a href="./withdraw.php"> <button type="button" class="btn-lg btn-primary btn-block">Withdraw</button></a>
<br>
<a href="./load.php"> <button type="button" class="btn-lg btn-primary btn-block">Change Receive Address</button></a>
<br>
</div>
    <div class="col-lg-4 my-4">
     
    <div class="card text-white bg-info mb-1">
      <div class="card-header">Settings</div>
      <div class="card-body">
        <h6 class="card-title">
          <form action="#" method="post">

            <label>Bet Amount (0.1 - 1)</label>
            <input class="form-control mr-sm-2" id="bet_slider" type="range" min="0.1" max="10" step="0.1" value="<?php echo $_SESSION['amount_pay'];?>" name="amount_pay">
            <p>Value:<span id="bet_amount"></span></p><p><div class="rounded-pill btn btn-danger">Reward:<span id="reward_amount"></span></div></p>
            <label>Guess Dice Number (1-6) </label>
            <div class="stepper-widget">
            <input class="btn btn-danger js-qty-input" type="text" name="num" maxlength="3" style="max-width: 3rem;" readonly="readonly" value="<?php echo $_SESSION['selected'];?>" required/> <button type="button" class="js-qty-down btn btn-warning ">-</button><button type="button" class="js-qty-up btn btn-warning mx-1">+</button>
            </div>
            <input class="btn btn-primary btn-block my-2" type="submit" value="Change">
          </form>
        </h6>
      </div>
    </div>

    <div class='alert alert-info' role='alert'>
      <hr>
      <p>Bet Amount:<b><?php echo $_SESSION['amount_pay'];?></b></p>
      <p>Guess Value:<b><?php echo $_SESSION['selected'];?></b></p>

    <p>If either dice is equal to guess, win <b>bet x3</b></p>
  
    <div id="roll-msg"></div>
    <button id="btn-roll" class="btn-roll btn-lg btn-info btn-block">Roll Dice</button><hr>
    <!--
    <div class="progress">
    <div id="loading" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>-->
    <div class="row">
    <div class="col"><img src="" alt="Dice" class="dice1" width="50" height="50" style="display:none"></div>
    <div class="col"><img src="" alt="Dice" class="dice2" width="50" height="50" style="display:none"></div>
    </div>
    <h2 id="result"></h2>
</div>
</div>
<div class="col-lg-4 my-4">

    <div class="card text-white bg-dark mb-1">
      <div class="card-header">Nerva Block Height</div>
      <div class="card-body" style="max-height: 3rem;">
        <h6 class="card-title" id="block-height" ></h6>
      </div>
      <div class="card-header">Server Balance</div>
      <div class="card-body" style="max-height: 3rem;">
        <h6 class="card-title" id="pot_balance"></h6>
      </div>
   
      <div class="card-header">Unlocked Balance</div>
      <div class="card-body" style="max-height: 3rem;">
        <h6 class="card-title" id="pot_unlocked"></h6>
      </div>
      <div class="card-header">Total In Transactions</div>
      <div class="card-body" style="max-height: 3rem;">
        <h6 class="card-title" id="in-tx"></h6>
      </div>
   
      <div class="card-header">Total Out Transactions</div>
      <div class="card-body" style="max-height: 3rem;">
        <h6 class="card-title" id="out-tx"></h6>
      </div>
    </div>    

    <div class="card text-white bg-primary mb-1">
      <div class="card-header">Player Balance Remaining</div>
      <div class="card-body">
        <h5 class="card-title" id="payed"><?php echo round($_SESSION['payed'],6);?> XNV</h5>
      </div>
   
      <div class="card-header">Won</div>
      <div class="card-body">
        <h5 class="card-title" id="won"><?php echo round($_SESSION['won'],6);?> XNV</h5>
      </div>
    </div>   
  </div>
</div>
<script src="./app.js"></script>




<div class="row">
  <div class="card-deck mb-3 text-center">
    <div class="col-sm-12 col-md-6 col-lg-3  mb-2">
      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Mining Nerva</h4>
        </div>
        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
          Solo CPU mining means everyone with a computer has a chance to mine blocks.
          </ul>
          <a href="https://getnerva.org/#downloads"><button type="button" class="btn btn-lg btn-block btn-outline-primary">Start mining</button></a>
        </div>
      </div>
    </div>
  
    <div class="col-lg-3 col-md-6 col-sm-12 mb-2">

      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Shop with Nerva</h4>
        </div>

        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
              After receiving your drop of nerva you can now spend it here
          </ul>
          <a href="https://shop.yaslabs.com"> <button type="button" class="btn btn-lg btn-block btn-outline-primary">Shop</button></a>
        </div>
      </div>

    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 mb-2">

      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Statistics</h4>
        </div>
        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
            Get latest news about nerva and more.
          </ul>
          <a href="https://freeboard.io/board/EV5-se"> <button type="button" class="btn btn-lg btn-block btn-outline-primary">Statistics</button></a>

          
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('.stepper-widget').stepper({
    upSelector: '.js-qty-up',
    downSelector: '.js-qty-down',
    inputSelector: '.js-qty-input',
    disabledClass:'disabled',

    maxQty: 6,
    minQty: 1,
    step: 1
  });

  var slider = document.getElementById("bet_slider");
  var output = document.getElementById("bet_amount");
  var reward = document.getElementById("reward_amount");
  output.innerHTML = slider.value;
  reward.innerHTML = +(slider.value * 3).toPrecision(3);
  slider.oninput = function() {
  output.innerHTML = this.value;
  reward.innerHTML = +(this.value*3).toPrecision(3);
  }
  
function loadInit() {
  jQuery(document).ready(function () {	
			jQuery.get('./gateway.php',function (response) {
            if(typeof response === 'string'){
              response = JSON.parse(response);
            }       
			new QRCode(document.getElementById("qrcode"), response.integrated_address);
			 $( "#payed" ).html(response.payed+' XNV');  
            $('#int_add').html(response.integrated_address);
			
            $('#pot_balance').html(response.pot_balance+' XNV');
            $('#pot_unlocked').html(response.pot_unlocked+' XNV');
            $('#block-height').html(response.height);
            $('#in-tx').html(response.in_transfers);
            $('#out-tx').html(response.out_transfers);
        },"json"
      )
    });
}
function loadData() {
  jQuery(document).ready(function () {	
			jQuery.get('./gateway.php',function (response) {
            if(typeof response === 'string'){
              response = JSON.parse(response);
            }
            $( "#payed" ).html(response.payed+' XNV');  
            $('#int_add').html(response.integrated_address);
			
            $('#pot_balance').html(response.pot_balance+' XNV');
            $('#pot_unlocked').html(response.pot_unlocked+' XNV');
            $('#block-height').html(response.height);
            $('#in-tx').html(response.in_transfers);
            $('#out-tx').html(response.out_transfers);
        },"json"
      )
    });
}
loadInit();
interval = setInterval(loadData,10000);
</script>
</body>
</html>

