<?php
session_start();
include_once('./checkhttps.php');
/*
if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || 
   $_SERVER['HTTPS'] == 1) ||  
   isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&   
   $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
{
   $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
   header('HTTP/1.1 301 Moved Permanently');
   header('Location: ' . $redirect);
   exit();
}*/
$amount_pay=$_SESSION['amount_pay'];

if(!isset($_SESSION['burnt'])){
    $_SESSION['burnt']=0;
}
if(!isset($_SESSION['totalPayedAmount'])){
    $_SESSION['totalPayedAmount']=0;
}
if(!isset($_SESSION['won'])){
    $_SESSION['won']=0;
}
$remaining=$_SESSION['totalPayedAmount']-$_SESSION['burnt'];
$result="";
if($remaining >= $amount_pay && isset($_SESSION['selected']) && $amount_pay<=10){
    $num1 = mt_rand(1, 6);
    $num2 = mt_rand(1, 6);
    
    if($_SESSION['selected']==$num1 && $_SESSION['selected']==$num2){
       // $_SESSION['won'] += $amount_pay*3;
       // $result="Won:".$amount_pay*3;
	    $_SESSION['won'] += $amount_pay*3;
		$_SESSION['burnt'] += $amount_pay;  
        $result="Won:".$amount_pay*3;
    }elseif($_SESSION['selected']==$num1 ||$_SESSION['selected']==$num2){
        $_SESSION['won'] += $amount_pay*3;
		$_SESSION['burnt'] += $amount_pay;  
        $result="Won:".$amount_pay*3;
    }elseif($num1!=$num2){
        $_SESSION['burnt'] += $amount_pay;   
        $result="Try again"; 
    }elseif($num1==$num2){
        //$result="Free pass"; 
      //  $_SESSION['burnt'] += $amount_pay;   
        $result="Try again"; 
    }
    
    $_SESSION['num1']=$num1;
    $_SESSION['num2']=$num2;
  
    $remaining=$_SESSION['totalPayedAmount']-$_SESSION['burnt'];
    //echo $num;
    header('Content-Type: application/json');
    echo json_encode(array(
        'payed'=>round($remaining,1),
        'won'=>round($_SESSION['won'],1),
        'dice1'=>$num1,
        'dice2'=>$num2,
        'result'=>$result,
    ));
    
}else{
    $msg="";
    if($remaining<$amount_pay){
        $msg="Balance insufficient!<br>";
    }
    if(!isset($_SESSION['selected'])){
        $msg = $msg."Select dice number!<br>";
    }
    header('Content-Type: application/json');
    echo json_encode(array(
        'err'=>$msg,
        'dice1'=>'0',
        'dice2'=>'0'    
        
    ));
}

?>