<?php
session_start();
include_once('./checkhttps.php');
require_once('./nerva/library.php');
$nerva_daemon = new Nerva_Library('localhost', WALLET_PORT);
require_once('./header.php');

if(isset($_SESSION['payed_payment_id'])){
    check_for_spend($_SESSION['payed_payment_id']);
}

if(isset($_SESSION['trasfered'])){
    if($_SESSION['trasfered']==true){
        reset_session();
        header( "refresh:10;url=./load.php" );
        echo "<br>Refresh in 10 seconds";
        exit();
    }
}
$amount_pay=0.1;
$wallet_amount=$nerva_daemon->getbalance();
$real_wallet_amount = $wallet_amount['balance'] / 1000000000000;
$real_amount_rounded = round($real_wallet_amount, 1);

$unlocked_wallet_amount = $wallet_amount['unlocked_balance'] / 1000000000000;
$unlocked_amount_rounded = round($unlocked_wallet_amount, 1);
if(isset($_SESSION['payment_id'])){
    $payment_id=$_SESSION['payment_id'];
    $integrated_address=$nerva_daemon->make_integrated_address($payment_id);
    $_SESSION['integrated_address']=$integrated_address['integrated_address'];
   
}else{
    $payment_id = bin2hex(openssl_random_pseudo_bytes(8));
    $_SESSION['payment_id']=$payment_id;
    $integrated_address=$nerva_daemon->make_integrated_address($payment_id);
    $_SESSION['integrated_address']=$integrated_address['integrated_address'];
  

}
if(!isset($_SESSION['burnt'])){
    $_SESSION['burnt']=0;
}
verify_payment($payment_id,$amount_pay);
$in_transfers=$nerva_daemon->get_transfers("in",true);
$out_transfers=$nerva_daemon->get_transfers("out",true);
$failed_transfers=$nerva_daemon->get_transfers("failed",true);
$pool_transfers=$nerva_daemon->get_transfers("pool",true);
$pending_transfers=$nerva_daemon->get_transfers("pending",true);

header('Content-Type: application/json');
echo json_encode(array(
    'payed'=>round($_SESSION['payed'],1),
    'integrated_address'=>$_SESSION['integrated_address'],
    'pot_balance'=>$real_amount_rounded,
    'pot_unlocked'=>$unlocked_amount_rounded,
    'height'=> $nerva_daemon->getheight()["height"],
    'in_transfers'=>sizeof($in_transfers["in"]),
    'out_transfers'=>sizeof($out_transfers["out"]),
));
function verify_payment($payment_id, $amount)
{
    global $nerva_daemon;
    $amount_atomic_units = $amount * 1000000000000;
    
    $get_payments_method = $nerva_daemon->get_payments($payment_id);
    if (isset($get_payments_method["payments"][0]["amount"])) {
        $totalPayed = $get_payments_method["payments"][0]["amount"];
        $outputs_count = count($get_payments_method["payments"]); // number of outputs recieved with this payment id
        $output_counter = 1;

        while($output_counter < $outputs_count){
            $totalPayed += $get_payments_method["payments"][$output_counter]["amount"];
            $output_counter++;
            
        }
        //added
        $totalPayedAmount=$totalPayed/1000000000000;

        if($totalPayed >= $amount_atomic_units){
            $tx_height = $get_payments_method["payments"][$outputs_count-1]["block_height"];
            $get_height = $nerva_daemon->getheight();
            $bc_height = $get_height["height"] - 1;
            $confirmations = ($bc_height - $tx_height) + 1;

            if($confirmations>=0){
                $_SESSION['totalPayedAmount']=$totalPayedAmount;
                $remaining=$totalPayedAmount - $_SESSION['burnt'];
                $_SESSION['payed'] = round($remaining,1);
                if($_SESSION['payed']<0) $_SESSION['payed']=0;
        
            }
            
        }
    }
}
?>