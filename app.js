var scores, roundScore, activePlayer, gamePlaying;

init();

var elem = document.getElementById("loading");
var width = 0;
var timer_id;
function dice_roll(){
  
  jQuery(document).ready(function () {
    jQuery.get('./roll.php', function (response) {
      clearInterval(timer_id);
      document.getElementById("btn-roll").disabled = false;
     
      if (typeof response === 'string') {
        response = JSON.parse(response);
      }
      if (response.dice1 != '0' && response.dice2 != '0') {
  
        $("#payed").html(response.payed + ' XNV');
        $("#won").html(response.won + ' XNV');
        var diceDOM1 = document.querySelector('.dice1')
        diceDOM1.style.display = 'block';
        diceDOM1.src = 'dice-' + response.dice1 + '.png';
        var diceDOM2 = document.querySelector('.dice2')
        diceDOM2.style.display = 'block';
        diceDOM2.src = 'dice-' + response.dice2 + '.png';
        $("#roll-msg").html("");
        $("#result").html(response.result);
      } else {
        $("#roll-msg").html(response.err);
        document.querySelector('.dice1').style.display = 'none';
        document.querySelector('.dice2').style.display = 'none';
      }
      
    }, "json",
    )
  });
  
}

document.querySelector('.btn-roll').addEventListener('click', function () {
  document.getElementById("btn-roll").disabled = true;
  setTimeout(dice_roll,1000);
  timer_id=setInterval(function(){

    var random =Math.floor(Math.random() * 5) +1; 
    var diceDOM = document.querySelector('.dice1')
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + random + '.png';
    var random =Math.floor(Math.random() * 5) +1; 
    var diceDOM = document.querySelector('.dice2')
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + random + '.png';
  },100)
  
});


function init() {
  activePlayer = 0;
  
};
